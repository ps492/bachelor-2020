from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return HttpResponse("<!DOCTYPE html><h1>Hello World!</h1>")

def detail(request, question_id):
    return HttpResponse(f"You're looking at the question {question_id}")

def results(request, question_id):
    response = 